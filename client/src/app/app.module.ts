import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FilesComponent } from './components/files/files.component';
import { SectionHeaderComponent } from './components/section-header/section-header.component';
import { TableComponent } from './components/files/table/table.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app.routing';
import { RegisterComponent } from './components/register/register.component';
import { FileUploadModalComponent } from './components/file-upload-modal/file-upload-modal.component';
import { AccountComponent } from './components/account/account.component'; 
import { EditCredentialsModalComponent } from './components/account/edit-credentials-modal/edit-credentials-modal.component';

import { RemoveHostDirective } from './directives/remove-host.directive';
import { AsyncEmailValidatorDirective } from './directives/custom-validators/async-email-validator.directive';

import { MainHttpService } from './services/main-http.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

import { IsAuthenticatedGuard } from './guards/is-authenticated.guard';
import { IsNotAuthenticatedGuard } from './guards/is-not-authenticated.guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FilesComponent,
    SectionHeaderComponent,
    TableComponent,
    RemoveHostDirective,
    LoginComponent,
    RegisterComponent,
    FileUploadModalComponent,
    AccountComponent,
    EditCredentialsModalComponent,
    AsyncEmailValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    MainHttpService,
    AuthService,
    UserService,
    IsAuthenticatedGuard,
    IsNotAuthenticatedGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    FileUploadModalComponent,
    EditCredentialsModalComponent
  ]
})
export class AppModule { }
