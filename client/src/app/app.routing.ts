import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilesComponent } from './components/files/files.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { IsAuthenticatedGuard } from './guards/is-authenticated.guard';
import { IsNotAuthenticatedGuard } from './guards/is-not-authenticated.guard';
import { AccountComponent } from './components/account/account.component';



const appRoutes: Routes = [
    { 
        path: '',
        redirectTo: 'files',
        pathMatch: 'full',
        canActivate: [IsAuthenticatedGuard]
    },
    {
        path: 'files',
        component: FilesComponent,
        canActivate: [IsAuthenticatedGuard]
    },
    {
        path: 'account',
        component: AccountComponent,
        canActivate: [IsAuthenticatedGuard]
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [IsNotAuthenticatedGuard]
    },
    {
        path: 'register',
        component: RegisterComponent,
        canActivate: [IsNotAuthenticatedGuard]
    },
    { path: '**', redirectTo: 'files' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}