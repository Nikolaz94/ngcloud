import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MainHttpService } from './main-http.service';
import { Token } from '../classes/token';


import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../classes/user';
import { LoginCredentials } from '../classes/login-credentials';
import { RegisterCredentials } from '../classes/register-credentials';
import { UpdateCredentials } from '../classes/update-credentials';

@Injectable()
export class AuthService {

  private urlAuth: string = '/api/auth';

  public token: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public user: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    private mainHttpService: MainHttpService,
    private router: Router
  ) {
    if(this.isLoggedIn()) {
      this.user.next(this.userCredentials());
      this.loggedIn.next(true);
      // this.router.navigate(['/files']);
    } else {
      // this.router.navigate(['/login']);
    }
  }

  getToken(): string {
    return Token.getToken();
  }

  isLoggedIn(): boolean {
    let token = this.getToken();
    let payload: any;
    if(token) {
      payload = Token.parseToken(token);
      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  userCredentials(): any {
    if(this.isLoggedIn()) {
      let token = this.getToken();
      let payload: any = Token.parseToken(token);
      delete payload['exp'];
      delete payload['iap'];
      return payload;
    }
    return null;
  }

  saveToken(token: string): void {
    Token.saveToken(token);
    this.token.next(token);
    this.loggedIn.next(true);
    this.user.next(this.userCredentials());
  }

  login(credentials: LoginCredentials, dataHandler?: (item: User) => void, errorHandler?: (error: any) => void): void {
    this.mainHttpService.post<LoginCredentials>(this.urlAuth + '/login', credentials)
      .subscribe((data: { token: string })=>{
        this.saveToken(data.token);
        this.router.navigate(['/account']);
        dataHandler(this.userCredentials());
      }, (error: any)=>{
        errorHandler(error);
      })
  }

  register(credentials: RegisterCredentials, dataHandler?: (item: User) => void, errorHandler?: (error: any) => void): void {
    this.mainHttpService.post<RegisterCredentials>(this.urlAuth + '/register', credentials)
      .subscribe((data: { token: string })=>{
        this.saveToken(data.token);
        this.router.navigate(['/account']);
        dataHandler(this.userCredentials());
      }, (error: any)=>{
        errorHandler(error);
      })
  }

  updateUserCredentials(newCredentials: UpdateCredentials, dataHandler?: (item: User) => void, errorHandler?: (error: any) => void): void {
    this.mainHttpService.post<UpdateCredentials>(this.urlAuth + '/update', newCredentials)
    .subscribe((data: { token:string })=>{
      this.saveToken(data.token);
      this.router.navigate(['/account']);
      dataHandler(this.userCredentials());
    }, (error: any)=>{
      errorHandler(error);
    })
  }

  logout(): void {
    Token.removeToken();
    this.token.next(null);
    this.loggedIn.next(false);
    this.user.next(null);
    this.router.navigate(['/login']);
  }

}
