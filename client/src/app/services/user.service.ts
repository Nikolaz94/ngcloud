import { Injectable } from '@angular/core';
import { User } from '../classes/user';
import { MainHttpService } from './main-http.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

  private urlUser: string = '/api/users'

  constructor(private mainHttpService: MainHttpService) { }

  getUserByEmail(userEmail: string): Promise<User[]> {
    var userEmail: string = userEmail.trim().replace('@','%40');
    userEmail = '^'+userEmail+'$';
    return this.mainHttpService.get<User[]>(this.urlUser + '?email=' + userEmail).toPromise(); 
  }
}
