import { LoginCredentials } from "./login-credentials";

export class RegisterCredentials extends LoginCredentials {
    constructor(
        public firstName: string,
        public lastName: string,
        email: string,
        password: string
    ) {
        super(email, password)
    }    
}
