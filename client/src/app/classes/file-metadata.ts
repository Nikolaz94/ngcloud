export class FileMetadata {
    constructor(
        public name: string,
        public uploadDate: string,
        public sizeInMb: number 

    ) {

    }

    setImage() {
        let split: string[] = this.name.split(".");
        let path: string = "../../../assets/images/";
    
        let imageName: string = "";
        
        if(split.length == 1) {
          imageName = "file.png";
        } else {
          let lastStr = split[split.length - 1];
          switch(lastStr) {
            case "jpg":
              imageName = "jpg.png";
              break;
            case "mp3":
              imageName = "mp3.png";
              break;          
            case "pdf":
              imageName = "pdf.png";
              break;          
            case "ppt":
              imageName = "ppt.png";
              break;
            case "pptx":
              imageName = "ppt.png";
              break;              
            case "txt":
              imageName = "txt.png";
              break;   
            case "doc":
              imageName = "word.png";
              break;
            case "docx":
              imageName = "word.png";
              break;                          
            case "xls":
              imageName = "xls.png";
              break;
            case "xlsx":
              imageName = "xls.png";
              break;                        
            default:
              imageName = "file.png"
          }
        }
    
        return path + imageName;
      }

}
