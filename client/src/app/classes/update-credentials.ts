import { RegisterCredentials } from "./register-credentials";

export class UpdateCredentials extends RegisterCredentials {
    constructor(
        public _id: string,
        firstName: string,
        lastName: string,
        email: string,
        password: string
    ) {
        super(firstName, lastName, email, password)
    }
}
