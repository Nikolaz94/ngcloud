export class Token {
    static parseToken(token: string): any {
        let payload: any;
        let data: any;
        data = token.split('.')[1]
        data = window.atob(data);
        payload = JSON.parse(data);
        return payload;
    }

    static saveToken(token: string): void {
        window.localStorage['ngcloud-token'] = token;
    } 

    static getToken(): string {
        return window.localStorage['ngcloud-token'];
    } 

    static removeToken(): void {
        window.localStorage.removeItem('ngcloud-token');
    }
}


