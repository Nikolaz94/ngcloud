import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from '../../services/auth.service';
import { User } from '../../classes/user';
import { EditCredentialsModalComponent } from './edit-credentials-modal/edit-credentials-modal.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  
  user: User = null;

  constructor(
    private authService: AuthService,
    public modalService: NgbModal
  ) { }

  ngOnInit() {
    this.subscribeForUser();
  }

  subscribeForUser() {
    this.authService
      .user.subscribe((user: User) => {
        this.user = user;
      });
  }

  openEditCredentialsdModal() {
    const modalRef = this.modalService.open(EditCredentialsModalComponent, {backdrop: true});

    modalRef.result.then((result: any) => {
      console.log(result);
    }, (dismiss: any)=> {
      console.log(dismiss);
    });
  }

}
