import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { User } from '../../../classes/user';
import { AuthService } from '../../../services/auth.service';
import { UpdateCredentials } from '../../../classes/update-credentials';
import { asyncEmailValidator } from '../../../directives/custom-validators/async-email-validator.directive';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-edit-credentials-modal',
  templateUrl: './edit-credentials-modal.component.html',
  styleUrls: ['./edit-credentials-modal.component.css']
})
export class EditCredentialsModalComponent implements OnInit {

  public accountForm: FormGroup;
  private user: User = null;
  public changedInput : boolean = false;

  constructor(
    public activeModal: NgbActiveModal,
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.subscribeForUser();
    this.initAccountForm();
  }

  initAccountForm() {
    this.accountForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [Validators.required]),
      lastName: new FormControl(this.user.lastName, [Validators.required]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email],[asyncEmailValidator(this.userService, this.user.email)]),
      password: new FormControl('')
    });
  }

  subscribeForUser() {
    this.authService
      .user.subscribe((user: User) => {
        this.user = user;
      });
  }

  onInputChange(event: any) {
    if((this.user[event.target.id] != event.target.value) && !this.changedInput) {
      this.changedInput = true;
    }
    if((this.user[event.target.id] == event.target.value) && this.changedInput) {
      this.changedInput = false;
    }
  }

  saveChanges() {
    let newCredentials: UpdateCredentials = this.accountForm.value;
    newCredentials._id = this.user._id;
    this.authService.updateUserCredentials(newCredentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
    this.activeModal.close(this.accountForm.value);
  }
}