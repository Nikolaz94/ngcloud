import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCredentialsModalComponent } from './edit-credentials-modal.component';

describe('EditCredentialsModalComponent', () => {
  let component: EditCredentialsModalComponent;
  let fixture: ComponentFixture<EditCredentialsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCredentialsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCredentialsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
