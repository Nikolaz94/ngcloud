import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';

import { LoginCredentials } from '../../classes/login-credentials';
import { User } from '../../classes/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userForm: FormGroup;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  public showError: boolean = false;

  onSubmit() {
    let credentials: LoginCredentials = this.userForm.value;
    this.authService.login(credentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
  }
}
