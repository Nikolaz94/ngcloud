import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators, AbstractControl } from '@angular/forms';

import { RegisterCredentials } from '../../classes/register-credentials';
import { User } from '../../classes/user';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { asyncEmailValidator } from '../../directives/custom-validators/async-email-validator.directive';

function passwordConfirming(c: AbstractControl): any {
  if(!c.parent || !c) return;
  const pwd = c.parent.get('password');
  const cpwd= c.parent.get('confirmPassword')

  if(!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
      return { invalid: true };
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public userForm: FormGroup;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email],[asyncEmailValidator(this.userService)]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required, passwordConfirming])
    });
  }

  public showError: boolean = false;

  onSubmit() {
    let formValue: any = this.userForm.value;
    delete formValue['confirmPassword'];
    let credentials: RegisterCredentials = formValue;
    this.authService.register(credentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
    
  }

}
