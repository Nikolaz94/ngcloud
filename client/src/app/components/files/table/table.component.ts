import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FileMetadata } from '../../../classes/file-metadata';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input() tableData: FileMetadata[];

  @Output() downloadEmitter: EventEmitter<FileMetadata> = new EventEmitter<FileMetadata>();

  constructor() { }

  ngOnInit() {
  }

  download(item: FileMetadata) {
    this.downloadEmitter.emit(item);
  }


}
