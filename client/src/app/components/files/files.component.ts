import { Component, OnInit } from '@angular/core';

import data from './data';
import { FileMetadata } from '../../classes/file-metadata';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModalComponent } from '../file-upload-modal/file-upload-modal.component';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  public tableData: FileMetadata[] = data;

  constructor(
    public modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  callDownloadService(item: FileMetadata) {
    console.log(item);
  }

  openFileUploadModal() {
    const modalRef = this.modalService.open(FileUploadModalComponent, {backdrop: true, size: 'lg'});

    modalRef.result.then((result: any) => {
      console.log(result);
    }, (dismiss: any)=> {
      console.log(dismiss);
    });

  }

}
