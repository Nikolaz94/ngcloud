import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-file-upload-modal',
  templateUrl: './file-upload-modal.component.html',
  styleUrls: ['./file-upload-modal.component.css']
})
export class FileUploadModalComponent implements OnInit {

  public fileForm: FormGroup;

  constructor(public activeModal: NgbActiveModal, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.initFileForm();
  }


  initFileForm() {
    this.fileForm = new FormGroup({
      file: new FormControl(null, [Validators.required])
    });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.fileForm.patchValue({
          file: reader.result
        });
        this.cd.markForCheck();        
      }
    }
  }

  public uploadInProgress: boolean = false;
  public progressBarValue: number = 0;
  uploadFile() {
    console.log(this.fileForm.value);
    this.uploadInProgress = true;
    
    let timer = setInterval(()=>{
      this.progressBarValue++;
      if(this.progressBarValue >= 100) {
        clearInterval(timer);
      }
    }, 50)

  }

}
