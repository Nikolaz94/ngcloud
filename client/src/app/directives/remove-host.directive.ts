import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appRemoveHost]'
})
export class RemoveHostDirective {
  constructor(private el?: ElementRef) {}
  ngOnInit() {
      var nativeElement: HTMLElement = this.el.nativeElement,
          parentElement: HTMLElement = nativeElement.parentElement;
      while (nativeElement.firstChild) {
          parentElement.insertBefore(nativeElement.firstChild, nativeElement);
      }
      parentElement.removeChild(nativeElement);
  }
}
