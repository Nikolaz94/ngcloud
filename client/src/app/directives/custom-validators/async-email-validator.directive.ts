import { Directive } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AsyncValidatorFn, AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

export function asyncEmailValidator(userService: UserService, predefEmail?: string): AsyncValidatorFn {
  return(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return userService.getUserByEmail(control.value).then(users=>{
      if(predefEmail === control.value){
        return null;
      } else {
        return (users && Object.keys(users).length > 0) ? {"emailTaken": true} : null;
      }
    });
  };
}


@Directive({
  selector: '[emailTaken][formControlName]',
  providers: [{provide: NG_ASYNC_VALIDATORS,
    useExisting: AsyncEmailValidatorDirective,
    multi: true
  }]
})
export class AsyncEmailValidatorDirective {

  constructor(private userService: UserService) { }

  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return asyncEmailValidator(this.userService)(control);
  }
}
