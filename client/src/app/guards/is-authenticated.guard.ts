import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {

  private currentUrl: string = null;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { };

  canActivate() {
    this.currentUrl = this.router.url;
    if(this.authService.isLoggedIn()) {
      return true;
    } else {
      if(this.currentUrl != "/"){
        window.alert("You don't have premission to view this page!");
      }
      this.router.navigate(['/login']);
      return false;
    }
  }
}
