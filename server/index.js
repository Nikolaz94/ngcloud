var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var busboyBodyParser = require('busboy-body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(busboyBodyParser());

var config = require('./config/config');

require('./config/db');

require('./config/passport');

var routes = require('./routes/index');
app.use('/api', routes);

app.use(express.static(path.join(__dirname, config.root)));
var server = app.listen(config.port, function(){
    console.log("Listening at port ", config.port);
});

app.use("*", function(req, res){
    res.sendFile(path.join(__dirname, config.root + "/index.html"));
});