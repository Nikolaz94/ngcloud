var mongoose = require('mongoose');
var mongodbUri = 'mongodb://localhost:27017';

mongoose.connect(mongodbUri);

mongoose.connection.on('connected', function(){
    console.log("Connected to DB");
});

mongoose.connection.on('error', function(){
    console.log("DB error");
});

mongoose.connection.on('disconnected', function(){
    console.log("Disconnected from DB");
});

require('../models/user.model');
require('../models/file.model');