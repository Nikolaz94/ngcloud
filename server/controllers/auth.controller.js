var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports.register = function(req, res) {
    var user = new User({
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName
    });

    user.setPassword(req.body.password);
    user.setGravatar(req.body.email);

    var token = user.generateJwt();

    user.save(function(err){
        if(err){
            res.status(400).json(err);
            return;
        }
        res.status(200)
            .json({
                token: token
        });

    });
}

module.exports.login = function(req, res) {
    passport.authenticate('local', function(err, user, info){
        var token;
        if(err){
            res.status(404).json(err);
            return;
        }
        if(user) {
            token = user.generateJwt();
            res.status(200)
                .json({ token: token })
        } else {
            res.status(401).json(info);
        }
    })(req, res);
}

module.exports.update = function(req, res) {
    const userId = mongoose.Types.ObjectId(req.body._id);
    User.findOne({_id: userId}, function(err, data){
        var user = data;
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;
        if(req.body.password != "") {
            user.setPassword(req.body.password);
        }

        var token = user.generateJwt();

        user.save(function(err){
            if(err){
                res.status(400).json(err);
                return;
            }
            res.status(200)
                .json({
                    token: token
            });
    
        });
    });

}