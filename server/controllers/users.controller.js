var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports.getUsersByEmail = function(req, res) {
    var qEmail = req.query.email;
    qEmail = qEmail.substring(1,qEmail.length-1);
    User.find({ email : qEmail }, function(err, users){
        var usersEmail = {};
        users.forEach(function(user) {
            usersEmail[user._id] = user;
        });
        res.send(usersEmail);
    });
}