var express = require('express');
var router = express.Router()

var UsersController = require('../controllers/users.controller');

console.log("users.routes.js");

router.route('')
    .get(UsersController.getUsersByEmail);

module.exports = router;