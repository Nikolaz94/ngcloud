var express = require('express');
var router = express.Router();

var authRoutes = require('./auth.routes');
var filesRoutes = require('./files.routes');
var userRoutes = require('./users.routes');

console.log("index.js"); 


router.use('/auth', authRoutes);
router.use('/users', userRoutes);
router.use('/files', filesRoutes);

module.exports = router;