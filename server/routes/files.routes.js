var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var auth = jwt({
    secret: 'MY_SECRET',
    userProperty: 'payload'
});

var filesController = require('../controllers/files.controller');

router.route('/')
    .get(auth, filesController.getAllByUser)
    .post(auth, filesController.upload)

router.route('/:id')
    .get(filesController.download)

module.exports = router;

